from dataclasses import fields
from pyexpat import model
from rest_framework import serializers
from .models import User_client, Contract, Event
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.models import User



class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
	@classmethod
	def get_token(cls, user):
		token = super().get_token(user)
		token['username'] = user.username
		return token



class UserClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = User_client
		exclude = ['sale_contact']



class ContractSerializer(serializers.ModelSerializer):
	class Meta:
		model = Contract
		fields = '__all__'



class EventClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Event
		fields = '__all__'