

# Third-party
from api.serializers import UserClientSerializer, ContractSerializer, EventClientSerializer, MyTokenObtainPairSerializer
from rest_framework import status, viewsets
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response


#App
from .models import *
from .permissions import CustomPermission, CustomEventPermission



class MyTokenObtainPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = MyTokenObtainPairSerializer



class UserClientViewSet(viewsets.ModelViewSet):
    permission_classes = (CustomPermission,)
    queryset = User_client.objects.all()
    serializer_class = UserClientSerializer
    filterset_fields = ['last_name', 'email']

    def create(self, request):
        serializer = UserClientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(sale_contact=request.user)
        return Response(serializer.data)



class ContractViewSet(viewsets.ModelViewSet):
    permission_classes = (CustomPermission,)
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    filterset_fields = ['user_client__last_name', 'user_client__email', 'created', 'amount']

    def create(self, request):
        serializer = ContractSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(sale_contact=request.user)
            id_client = serializer['user_client'].value
            User_client.objects.filter(id=id_client).update(potential_client=False)
        return Response(serializer.data)



class EventViewSet(viewsets.ModelViewSet):
    permission_classes = (CustomEventPermission,)
    queryset = Event.objects.all()
    serializer_class = EventClientSerializer
    filterset_fields = ['user_client__last_name', 'user_client__email']
