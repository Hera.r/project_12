
# Django
from os import pread
from django.db import models
from django.conf import settings



EVENTS_CHOICES = (('début', 'Début'), ('en cours', 'En cours'), ('terminé', 'Terminé',))


class User_client(models.Model): # To do: changer en CamelCase les noms de class
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	email = models.EmailField(max_length=254)
	phone = models.CharField(max_length=16, null=True, blank=True)
	company_name = models.CharField(max_length=250)
	created = models.DateTimeField(auto_now_add=True, null=True)
	updated = models.DateTimeField(auto_now=True, null=True)
	sale_contact = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name="Equipe vente")
	potential_client = models.BooleanField("Potentiel client", default=True)

	class Meta:
		verbose_name = "Client"
		verbose_name_plural = "Clients"

	def __str__(self) -> str:
		return f"{self.first_name}-{self.last_name}"

	def name_client(self):
		return f"{self.first_name}-{self.last_name}"


class Event(models.Model):
	user_client = models.ForeignKey(User_client, on_delete=models.CASCADE)
	created = models.DateTimeField("Date création",auto_now_add=True, null=True)
	updated = models.DateTimeField(auto_now=True, null=True)
	contact_support = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	events_status = models.CharField("Statut", choices=EVENTS_CHOICES, default='début', max_length=25)
	attendees = models.IntegerField("Invitées", help_text="choisir le nombre d'inviter")
	event_date = models.DateTimeField(auto_now_add=True, null=True)
	notes = models.TextField(max_length=300, blank=True, null=True)

	class Meta:
		verbose_name = "Evenement"
		verbose_name_plural = "Evenements"

	def __str__(self) -> str:
		return f"{self.user_client.first_name}-{self.user_client.last_name} | {self.user_client.company_name}"

	def name_client(self):
		return f"{self.first_name}-{self.last_name}"



class Contract(models.Model):
	sale_contact = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name="Equipe vente")
	user_client = models.ForeignKey(User_client, on_delete=models.CASCADE)
	created = models.DateTimeField("Date création", auto_now_add=True, null=True)
	updated = models.DateTimeField(auto_now=True, null=True)
	amount = models.FloatField(default=1000)
	payment_due = models.DateTimeField(auto_now_add=True, null=True)
	contract_signed = models.BooleanField("Signature contrat", default=True)

	class Meta:
		verbose_name = "Contrat"
		verbose_name_plural = "Contrats"

	def __str__(self) -> str:
		return f"{self.user_client.first_name}-{self.user_client.last_name} | {self.user_client.company_name}"

	def name_client(self):
		return f"{self.first_name}-{self.last_name}"

	@property
	def amount_euro(self):
		return f"{self.amount} €"