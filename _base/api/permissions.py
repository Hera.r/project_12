from pickle import OBJ
from rest_framework.permissions import BasePermission, DjangoModelPermissions, SAFE_METHODS



class CustomPermission(DjangoModelPermissions):
	message = 'access denied for non-authors or contributors'

	def has_permission(self, request, view):
		return request.user and request.user.is_authenticated
	
	def has_object_permission(self, request, view, obj):
		if request.method in SAFE_METHODS:
			return True
		return obj.sale_contact == request.user
		
		
		
		
		
class CustomEventPermission(DjangoModelPermissions):
	message = 'access denied for non-authors or contributors'

	def has_permission(self, request, view):
		return request.user and request.user.is_authenticated
	
	def has_object_permission(self, request, view, obj):
		if request.method in SAFE_METHODS:
			return True		
		return obj.contact_support == request.user
