#Django
from posixpath import basename
from django.urls import path, include

#Third-party
from rest_framework.routers import DefaultRouter

#App
from api import views
from api.views import MyTokenObtainPairView


router = DefaultRouter()

router.register('user_client', views.UserClientViewSet, basename="user_client")
router.register('contract', views.ContractViewSet, basename="contract")
router.register('event', views.EventViewSet, basename="event")

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
    path('login/', MyTokenObtainPairView.as_view(), name='token_obtain_pair')
]
