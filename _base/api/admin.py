from django.contrib import admin

# App
from .models import *



@admin.register(User_client)
class UserClientAdmin(admin.ModelAdmin):
	model = User_client
	list_display = ('name_client', 'email', 'sale_contact', 'potential_client')

	def name_client(self, obj):
		return obj.__str__()
	name_client.short_description = "Nom client"



@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
	model = Contract
	list_display = ('name_client', 'sale_contact',
					'created', 'amount_euro')

	def name_client(self, obj):
		return obj.__str__()
	name_client.short_description = "Nom client"



@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
	model = Event
	list_display = ('name_client', 'created',
					'attendees', 'events_status')
	fields = ("user_client", 'contact_support', 'attendees', 'notes')

	def formfield_for_foreignkey(self, db_field, request, **kwargs):
		if db_field.name == "user_client":
			kwargs["queryset"] = User_client.objects.filter(potential_client=False)
		return super().formfield_for_foreignkey(db_field, request, **kwargs)


	def name_client(self, obj):
		return obj.__str__()
	name_client.short_description = "Nom client"

